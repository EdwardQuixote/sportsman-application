package com.enock.sportapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.enock.sportapplication.DBUtils.DBContract;
import com.enock.sportapplication.DBUtils.DBHelper;

public class SponsorsActivity extends AppCompatActivity {

    private TextInputLayout tilSponsorsName;
    private TextInputLayout tilDOB;
    private TextInputLayout tilEmailAddress;
    private TextInputLayout tilLocation;
    private TextInputLayout tilPhoneNumber;

    private TextInputEditText tiedSponsorsName;
    private TextInputEditText tiedDOB;
    private TextInputEditText tiedEmailAddress;
    private TextInputEditText tiedLocation;
    private TextInputEditText tiedPhoneNumber;

    private Spinner spnGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sponsors);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to declare and initialize,
     * class variables and UI Objects referenced,
     * in the UI.
     * <p>
     * Called in this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        ArrayAdapter<String> adpGenderAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.saryGender));

        tilSponsorsName = (TextInputLayout) this.findViewById(R.id.tilSponsorsName);
        tilDOB = (TextInputLayout) this.findViewById(R.id.tilSponsorsDOB);
        tilEmailAddress = (TextInputLayout) this.findViewById(R.id.tilSponsorsEmailAddress);
        tilLocation = (TextInputLayout) this.findViewById(R.id.tilSponsorsLocation);
        tilPhoneNumber = (TextInputLayout) this.findViewById(R.id.tilSponsorsPhoneNumber);

        tiedSponsorsName = (TextInputEditText) this.findViewById(R.id.tiedSponsorsName);
        tiedDOB = (TextInputEditText) this.findViewById(R.id.tiedSponsorsDOB);
        tiedEmailAddress = (TextInputEditText) this.findViewById(R.id.tiedSponsorsEmailAddress);
        tiedLocation = (TextInputEditText) this.findViewById(R.id.tiedSponsorsLocation);
        tiedPhoneNumber = (TextInputEditText) this.findViewById(R.id.tiedSponsorsPhoneNumber);

        spnGender = (Spinner) this.findViewById(R.id.spnSponsorsGender);
        spnGender.setAdapter(adpGenderAdapter);

        Button btnViewSponsors = (Button) this.findViewById(R.id.btnSponsorsViewRecords);
        Button btnSave = (Button) this.findViewById(R.id.btnSponsorSave);
        btnViewSponsors.setOnClickListener(clkSponsors);
        btnSave.setOnClickListener(clkSponsors);

    }

    /**
     * Method to start View Records Activity -
     * to view all Sponsors.
     *
     * Called in this.clkSponsors.onClick();
     */
    private void codeToStartViewRecords() {

        Intent inStartViewRecords = new Intent(SponsorsActivity.this, ViewRecordsActivity.class);
        inStartViewRecords.putExtra("TABLE_NAME_TO_VIEW", DBContract.sTABLE_SPONSOR);
        startActivity(inStartViewRecords);
    }

    /**
     * Method to validate User's Input.
     * If appropriate; return true.
     * Else; return false.
     *
     * @return (Boolean)
     */
    private boolean codeToValidateUserInput() {

        if (tiedSponsorsName.getText().toString().equalsIgnoreCase("")) {
            tilSponsorsName.setError("Please Provide the Sponsors's Name.");

            tiedSponsorsName.requestFocus();

            return false;
        } else if (tiedDOB.getText().toString().equalsIgnoreCase("")) {
            tilDOB.setError("Please Provide the Sponsors's Date of Birth.");

            tiedDOB.requestFocus();

            return false;
        } else if (tiedEmailAddress.getText().toString().equalsIgnoreCase("")) {
            tilEmailAddress.setError("Please Provide the Sponsors's Email Address.");

            tiedEmailAddress.requestFocus();

            return false;
        } else if (tiedLocation.getText().toString().equalsIgnoreCase("")) {
            tilLocation.setError("Please Provide the Sponsors's Location.");

            tiedLocation.requestFocus();

            return false;
        } else if (tiedPhoneNumber.getText().toString().equalsIgnoreCase("")) {
            tilPhoneNumber.setError("Please Provide the Sponsors's Phone Number.");

            tiedPhoneNumber.requestFocus();

            return false;
        } else {

            tilSponsorsName.setError("");
            tilDOB.setError("");
            tilEmailAddress.setError("");
            tilLocation.setError("");
            tilPhoneNumber.setError("");

            return true;
        }
    }


    /**
     * View.OnClickListener interface for Sponsors.
     * Handles clicks on views on this Activity.
     * <p>
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private View.OnClickListener clkSponsors = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.btnSponsorsViewRecords:
                    codeToStartViewRecords();
                    break;

                case R.id.btnSponsorSave:

                    if (codeToValidateUserInput()) {

                        String sName = tiedSponsorsName.getText().toString();
                        String sDOB = tiedDOB.getText().toString();
                        String sEmailAddress = tiedEmailAddress.getText().toString();
                        String sLocation = tiedLocation.getText().toString();
                        String sPhoneNumber = tiedPhoneNumber.getText().toString();

                        String sGender = spnGender.getSelectedItem().toString();

                        new DBHelper(SponsorsActivity.this).codeToWriteToDatabase(DBContract.sTABLE_SPONSOR, sName, sGender, sDOB, sEmailAddress, sLocation, sPhoneNumber);

                        codeToStartViewRecords();
                    } else {
                        Snackbar.make(v, "Please Confirm The details you provided and try again.", Snackbar.LENGTH_LONG).show();
                    }

                    break;
            }
        }
    };
}
