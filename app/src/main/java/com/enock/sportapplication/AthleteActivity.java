package com.enock.sportapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.enock.sportapplication.DBUtils.DBContract;
import com.enock.sportapplication.DBUtils.DBHelper;

public class AthleteActivity extends AppCompatActivity {

    private TextInputLayout tilAthleteName;
    private TextInputLayout tilDOB;
    private TextInputLayout tilEmailAddress;
    private TextInputLayout tilLocation;
    private TextInputLayout tilPhoneNumber;

    private TextInputEditText tiedAthleteName;
    private TextInputEditText tiedDOB;
    private TextInputEditText tiedEmailAddress;
    private TextInputEditText tiedLocation;
    private TextInputEditText tiedPhoneNumber;

    private Spinner spnGender;

    @Override
         protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_athlete);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to declare and initialize,
     * class variables and UI Objects referenced,
     * in the UI.
     *
     * Called in this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        ArrayAdapter<String> adpGenderAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.saryGender));

        tilAthleteName = (TextInputLayout) this.findViewById(R.id.tilAthleteName);
        tilDOB = (TextInputLayout) this.findViewById(R.id.tilAthleteDOB);
        tilEmailAddress = (TextInputLayout) this.findViewById(R.id.tilAthleteEmailAddress);
        tilLocation = (TextInputLayout) this.findViewById(R.id.tilAthleteLocation);
        tilPhoneNumber = (TextInputLayout) this.findViewById(R.id.tilAthletePhoneNumber);

        tiedAthleteName = (TextInputEditText) this.findViewById(R.id.tiedAthleteName);
        tiedDOB = (TextInputEditText) this.findViewById(R.id.tiedAthleteDOB);
        tiedEmailAddress = (TextInputEditText) this.findViewById(R.id.tiedAthleteEmailAddress);
        tiedLocation = (TextInputEditText) this.findViewById(R.id.tiedAthleteLocation);
        tiedPhoneNumber = (TextInputEditText) this.findViewById(R.id.tiedAthletePhoneNumber);

        spnGender = (Spinner) this.findViewById(R.id.spnAthleteGender);
        spnGender.setAdapter(adpGenderAdapter);

        Button btnViewAthletes = (Button) this.findViewById(R.id.btnAthleteViewRecords);
        Button btnSave = (Button) this.findViewById(R.id.btnAthleteSave);
        btnViewAthletes.setOnClickListener(clkAthlete);
        btnSave.setOnClickListener(clkAthlete);

    }

    /**
     * Method to start View Records Activity.
     *
     * Called in: this.clkAthlete.onClick();
     */
    private void codeToStartViewRecords() {

        Intent inStartViewRecords = new Intent(AthleteActivity.this, ViewRecordsActivity.class);
        inStartViewRecords.putExtra("TABLE_NAME_TO_VIEW", DBContract.sTABLE_ATHLETE);
        startActivity(inStartViewRecords);

    }

    /**
     * Method to validate User's Input.
     * If appropriate; return true.
     * Else; return false.
     *
     * @return              (Boolean)
     */
    private boolean codeToValidateUserInput() {

        if (tiedAthleteName.getText().toString().equalsIgnoreCase("")) {
            tilAthleteName.setError("Please Provide the Athlete's Name.");

            tiedAthleteName.requestFocus();

            return false;
        } else if (tiedDOB.getText().toString().equalsIgnoreCase("")) {
            tilDOB.setError("Please Provide the Athlete's Date of Birth.");

            tiedDOB.requestFocus();

            return false;
        } else if (tiedEmailAddress.getText().toString().equalsIgnoreCase("")) {
            tilEmailAddress.setError("Please Provide the Athlete's Email Address.");

            tiedEmailAddress.requestFocus();

            return false;
        } else if (tiedLocation.getText().toString().equalsIgnoreCase("")) {
            tilLocation.setError("Please Provide the Athlete's Location.");

            tiedLocation.requestFocus();

            return false;
        } else if (tiedPhoneNumber.getText().toString().equalsIgnoreCase("")) {
            tilPhoneNumber.setError("Please Provide the Athlete's Phone Number.");

            tiedPhoneNumber.requestFocus();

            return false;
        } else {

            tilAthleteName.setError("");
            tilDOB.setError("");
            tilEmailAddress.setError("");
            tilLocation.setError("");
            tilPhoneNumber.setError("");

            return true;
        }
    }


    /**
     * View.OnClickListener interface for Athlete.
     * Handles clicks on views on this Activity.
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private View.OnClickListener clkAthlete = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.btnAthleteSave:

                    if (codeToValidateUserInput()) {

                        String sName = tiedAthleteName.getText().toString();
                        String sDOB = tiedDOB.getText().toString();
                        String sEmailAddress = tiedEmailAddress.getText().toString();
                        String sLocation = tiedLocation.getText().toString();
                        String sPhoneNumber = tiedPhoneNumber.getText().toString();

                        String sGender = spnGender.getSelectedItem().toString();

                        new DBHelper(AthleteActivity.this).codeToWriteToDatabase(DBContract.sTABLE_ATHLETE, sName, sGender, sDOB, sEmailAddress, sLocation, sPhoneNumber);

                        codeToStartViewRecords();
                    } else {
                        Snackbar.make(v, "Please Confirm The details you provided and try again.", Snackbar.LENGTH_LONG).show();
                    }

                    break;

                case R.id.btnAthleteViewRecords:
                    codeToStartViewRecords();
                    break;
            }
        }
    };
}
