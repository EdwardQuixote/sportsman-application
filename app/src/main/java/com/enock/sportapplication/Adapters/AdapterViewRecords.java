package com.enock.sportapplication.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.enock.sportapplication.Models.ModelSportsMan;
import com.enock.sportapplication.R;

import java.util.ArrayList;

/**
 * Adapter class for ListView View Records.
 *
 * Created by Edward Ndukui,
 * on Tuesday, 18-Jul-17,
 * at 8:50PM.
 */
public class AdapterViewRecords extends BaseAdapter {

    private Context coxContext;

    private ArrayList<ModelSportsMan> arylDetails;

    public AdapterViewRecords(Context context, ArrayList<ModelSportsMan> arrayListDetails) {
        this.coxContext = context;

        this.arylDetails = arrayListDetails;
    }

    @Override
    public int getCount() {
        return arylDetails.size();
    }

    @Override
    public Object getItem(int position) {
        return arylDetails.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderClass clsViewHolder = null;
        if (convertView == null) {

            convertView = LayoutInflater.from(coxContext).inflate(R.layout.rowlayout_view_records, parent, false);

            clsViewHolder = new ViewHolderClass(convertView);

            convertView.setTag(clsViewHolder);
        } else {
            clsViewHolder = (ViewHolderClass) convertView.getTag();
        }

        String sNameGender = arylDetails.get(position).getsName() + " - " + arylDetails.get(position).getsGender();
        String sDOB = "Date of Birth: " + arylDetails.get(position).getsDateOfBirth();
        String sEmailPhone = arylDetails.get(position).getsEmailAddress() + " - " + arylDetails.get(position).getsPhoneNumber();
        String sLocation = "Location: " + arylDetails.get(position).getsLocation();

        clsViewHolder.txtNameGender.setText(sNameGender);
        clsViewHolder.txtDOB.setText(sDOB);
        clsViewHolder.txtEmailPhone.setText(sEmailPhone);
        clsViewHolder.txtLocation.setText(sLocation);

        return convertView;
    }


    private static class ViewHolderClass {

        private TextView txtNameGender;
        private TextView txtDOB;
        private TextView txtEmailPhone;
        private TextView txtLocation;

        public ViewHolderClass(View itemView) {

            txtNameGender = (TextView) itemView.findViewById(R.id.txtRecordNameGender);
            txtDOB = (TextView) itemView.findViewById(R.id.txtRecordDOB);
            txtEmailPhone = (TextView) itemView.findViewById(R.id.txtRecordEmailPhone);
            txtLocation = (TextView) itemView.findViewById(R.id.txtRecordLocation);

        }
    }
}
