package com.enock.sportapplication.Models;

/**
 * Data Model Class for Sports Man (Athlete, Boxer, Sponsor).
 *
 * Created by Edward Ndukui,
 * on Tuesday, 18-Jul-17,
 * at 8:18PM.
 */
public class ModelSportsMan {

    private String sName;
    private String sGender;
    private String sDateOfBirth;
    private String sEmailAddress;
    private String sLocation;
    private String sPhoneNumber;

    private int iSportsManId;

    public int getiSportsManId() {
        return iSportsManId;
    }

    public void setiSportsManId(int iSportsManId) {
        this.iSportsManId = iSportsManId;
    }

    public String getsDateOfBirth() {
        return sDateOfBirth;
    }

    public void setsDateOfBirth(String sDateOfBirth) {
        this.sDateOfBirth = sDateOfBirth;
    }

    public String getsEmailAddress() {
        return sEmailAddress;
    }

    public void setsEmailAddress(String sEmailAddress) {
        this.sEmailAddress = sEmailAddress;
    }

    public String getsGender() {
        return sGender;
    }

    public void setsGender(String sGender) {
        this.sGender = sGender;
    }

    public String getsLocation() {
        return sLocation;
    }

    public void setsLocation(String sLocation) {
        this.sLocation = sLocation;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public String getsPhoneNumber() {
        return sPhoneNumber;
    }

    public void setsPhoneNumber(String sPhoneNumber) {
        this.sPhoneNumber = sPhoneNumber;
    }
}
