package com.enock.sportapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.enock.sportapplication.DBUtils.DBContract;
import com.enock.sportapplication.DBUtils.DBHelper;

public class BoxersActivity extends AppCompatActivity {

    private TextInputLayout tilBoxersName;
    private TextInputLayout tilDOB;
    private TextInputLayout tilEmailAddress;
    private TextInputLayout tilLocation;
    private TextInputLayout tilPhoneNumber;

    private TextInputEditText tiedBoxersName;
    private TextInputEditText tiedDOB;
    private TextInputEditText tiedEmailAddress;
    private TextInputEditText tiedLocation;
    private TextInputEditText tiedPhoneNumber;

    private Spinner spnGender;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boxers);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to declare and initialize,
     * class variables and UI Objects referenced,
     * in the UI.
     * <p>
     * Called in this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        ArrayAdapter<String> adpGenderAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, getResources().getStringArray(R.array.saryGender));

        tilBoxersName = (TextInputLayout) this.findViewById(R.id.tilBoxersName);
        tilDOB = (TextInputLayout) this.findViewById(R.id.tilBoxersDOB);
        tilEmailAddress = (TextInputLayout) this.findViewById(R.id.tilBoxersEmailAddress);
        tilLocation = (TextInputLayout) this.findViewById(R.id.tilBoxersLocation);
        tilPhoneNumber = (TextInputLayout) this.findViewById(R.id.tilBoxersPhoneNumber);

        tiedBoxersName = (TextInputEditText) this.findViewById(R.id.tiedBoxersName);
        tiedDOB = (TextInputEditText) this.findViewById(R.id.tiedBoxersDOB);
        tiedEmailAddress = (TextInputEditText) this.findViewById(R.id.tiedBoxersEmailAddress);
        tiedLocation = (TextInputEditText) this.findViewById(R.id.tiedBoxersLocation);
        tiedPhoneNumber = (TextInputEditText) this.findViewById(R.id.tiedBoxersPhoneNumber);

        spnGender = (Spinner) this.findViewById(R.id.spnBoxersGender);
        spnGender.setAdapter(adpGenderAdapter);

        Button btnViewBoxers = (Button) this.findViewById(R.id.btnBoxersViewRecords);
        Button btnSave = (Button) this.findViewById(R.id.btnBoxerSave);
        btnViewBoxers.setOnClickListener(clkBoxers);
        btnSave.setOnClickListener(clkBoxers);

    }

    /**
     * Method to start View Records Activity -
     * to view all Boxers.
     *
     * Called in this.clkBoxers.onClick();
     */
    private void codeToStartViewRecords() {

        Intent inStartViewRecords = new Intent(BoxersActivity.this, ViewRecordsActivity.class);
        inStartViewRecords.putExtra("TABLE_NAME_TO_VIEW", DBContract.sTABLE_BOXER);
        startActivity(inStartViewRecords);
    }

    /**
     * Method to validate User's Input.
     * If appropriate; return true.
     * Else; return false.
     *
     * @return (Boolean)
     */
    private boolean codeToValidateUserInput() {

        if (tiedBoxersName.getText().toString().equalsIgnoreCase("")) {
            tilBoxersName.setError("Please Provide the Boxer's Name.");

            tiedBoxersName.requestFocus();

            return false;
        } else if (tiedDOB.getText().toString().equalsIgnoreCase("")) {
            tilDOB.setError("Please Provide the Boxer's Date of Birth.");

            tiedDOB.requestFocus();

            return false;
        } else if (tiedEmailAddress.getText().toString().equalsIgnoreCase("")) {
            tilEmailAddress.setError("Please Provide the Boxer's Email Address.");

            tiedEmailAddress.requestFocus();

            return false;
        } else if (tiedLocation.getText().toString().equalsIgnoreCase("")) {
            tilLocation.setError("Please Provide the Boxer's Location.");

            tiedLocation.requestFocus();

            return false;
        } else if (tiedPhoneNumber.getText().toString().equalsIgnoreCase("")) {
            tilPhoneNumber.setError("Please Provide the Boxer's Phone Number.");

            tiedPhoneNumber.requestFocus();

            return false;
        } else {

            tilBoxersName.setError("");
            tilDOB.setError("");
            tilEmailAddress.setError("");
            tilLocation.setError("");
            tilPhoneNumber.setError("");

            return true;
        }
    }


    /**
     * View.OnClickListener interface for Boxers.
     * Handles clicks on views on this Activity.
     * <p>
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private View.OnClickListener clkBoxers = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.btnBoxersViewRecords:
                    codeToStartViewRecords();
                    break;

                case R.id.btnBoxerSave:

                    if (codeToValidateUserInput()) {

                        String sName = tiedBoxersName.getText().toString();
                        String sDOB = tiedDOB.getText().toString();
                        String sEmailAddress = tiedEmailAddress.getText().toString();
                        String sLocation = tiedLocation.getText().toString();
                        String sPhoneNumber = tiedPhoneNumber.getText().toString();

                        String sGender = spnGender.getSelectedItem().toString();

                        new DBHelper(BoxersActivity.this).codeToWriteToDatabase(DBContract.sTABLE_BOXER, sName, sGender, sDOB, sEmailAddress, sLocation, sPhoneNumber);

                        codeToStartViewRecords();
                    } else {
                        Snackbar.make(v, "Please Confirm The details you provided and try again.", Snackbar.LENGTH_LONG).show();
                    }

                    break;
            }
        }
    };
}
