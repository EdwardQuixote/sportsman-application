package com.enock.sportapplication.DBUtils;

import android.provider.BaseColumns;

/**
 * Database Contract class for Database.
 *
 * Created by Edward Ndukui,
 * on Tuesday, 18-Jul-17,
 * at 7:29PM.
 */
public class DBContract {

    //  DB Name and Version:
    public static final String sDB_NAME = "SPORT_APPLICATION";
    public static final int iDB_VERSION = 1;

    //  Tables' Names:
    public static final String sTABLE_ATHLETE = "ATHLETE";
    public static final String sTABLE_BOXER = "BOXER";
    public static final String sTABLE_SPONSOR = "SPONSOR";

    //  Create Table Statements:
    public static final String sCREATE_TABLE_ATHLETE = "CREATE TABLE IF NOT EXISTS " + sTABLE_ATHLETE + "("
            + DBColumns.sColAthleteID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DBColumns.sColName + " TEXT, "
            + DBColumns.sColGender + " TEXT, "
            + DBColumns.sColDOB + " TEXT, "
            + DBColumns.sColEmailAddress + " TEXT, "
            + DBColumns.sColLocation + " TEXT, "
            + DBColumns.sColPhoneNumber + " TEXT);";
    public static final String sCREATE_TABLE_BOXER = "CREATE TABLE IF NOT EXISTS " + sTABLE_BOXER + "("
            + DBColumns.sColBoxerID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DBColumns.sColName + " TEXT, "
            + DBColumns.sColGender + " TEXT, "
            + DBColumns.sColDOB + " TEXT, "
            + DBColumns.sColEmailAddress + " TEXT, "
            + DBColumns.sColLocation + " TEXT, "
            + DBColumns.sColPhoneNumber + " TEXT);";
    public static final String sCREATE_TABLE_SPONSOR = "CREATE TABLE IF NOT EXISTS " + sTABLE_SPONSOR + "("
            + DBColumns.sColSponsorID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DBColumns.sColName + " TEXT, "
            + DBColumns.sColGender + " TEXT, "
            + DBColumns.sColDOB + " TEXT, "
            + DBColumns.sColEmailAddress + " TEXT, "
            + DBColumns.sColLocation + " TEXT, "
            + DBColumns.sColPhoneNumber + " TEXT);";

    //      Select from Tables Statements:
    public static final String sSELECT_ALL_FROM_ATHLETE = "SELECT * FROM " + sTABLE_ATHLETE;
    public static final String sSELECT_ALL_FROM_BOXER = "SELECT * FROM " + sTABLE_BOXER;
    public static final String sSELECT_ALL_FROM_SPONSOR = "SELECT * FROM " + sTABLE_SPONSOR;

    //      Delete from Tables Statements:
    public static final String sDELETE_FROM_ATHLETE = "DELETE FROM "
            + sTABLE_ATHLETE
            + " WHERE "
            + DBColumns.sColAthleteID
            + " = ";
    public static final String sDELETE_FROM_BOXER = "DELETE FROM "
            + sTABLE_BOXER
            + " WHERE "
            + DBColumns.sColBoxerID
            + " = ";
    public static final String sDELETE_FROM_SPONSOR = "DELETE FROM "
            + sTABLE_SPONSOR
            + " WHERE "
            + DBColumns.sColSponsorID
            + " = ";



    public static abstract class DBColumns implements BaseColumns {

        //  Table Athletes Columns:
        public static final String sColAthleteID = "ATHLETE_ID";
        public static final String sColBoxerID = "BOXER_ID";
        public static final String sColSponsorID = "SPONSOR_ID";
        public static final String sColName = "NAME";
        public static final String sColGender = "GENDER";
        public static final String sColDOB = "DATE_OF_BIRTH";
        public static final String sColEmailAddress = "EMAIL_ADDRESS";
        public static final String sColLocation = "LOCATION";
        public static final String sColPhoneNumber = "PHONE_NUMBER";

    }
}
