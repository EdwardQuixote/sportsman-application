package com.enock.sportapplication.DBUtils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.enock.sportapplication.Models.ModelSportsMan;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Database Helper class for This Application's Database.
 *
 * Created by Edward Ndukui,
 * on Tuesday, 18-Jul-17,
 * at 7:51PM.
 */
public class DBHelper extends SQLiteOpenHelper {

    public DBHelper(Context context) {
        super(context, DBContract.sDB_NAME, null, DBContract.iDB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.codeToCreateTablesInDB(db);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        this.codeToCreateTablesInDB(db);
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onDowngrade(db, oldVersion, newVersion);
    }

    @Override
    public SQLiteDatabase getReadableDatabase() {
        return super.getReadableDatabase();
    }

    @Override
    public SQLiteDatabase getWritableDatabase() {
        return super.getWritableDatabase();
    }


    /**
     * Method to create Tables in Database.
     *
     * Called in:
     *          this.onCreate();
     *          this.onUpgrade();
     *
     * @param sqliteDatabase            (SQLiteDatabase)
     */
    private void codeToCreateTablesInDB(SQLiteDatabase sqliteDatabase) {
        Log.e("DBHelper", "codeToCreateTablesInDB() CALLED.");

        sqliteDatabase.execSQL(DBContract.sCREATE_TABLE_ATHLETE);
        Log.e("DBHelper", "Table ATHLETE CREATED!");

        sqliteDatabase.execSQL(DBContract.sCREATE_TABLE_BOXER);
        Log.e("DBHelper", "Table BOXER CREATED!");

        sqliteDatabase.execSQL(DBContract.sCREATE_TABLE_SPONSOR);
        Log.e("DBHelper", "Table SPONSOR CREATED!");
    }

    public void codeToWriteToDatabase(String tableName, String name, String gender, String dob, String emailAddress, String location, String phoneNumber) {

        ContentValues cvData = new ContentValues();
        cvData.put(DBContract.DBColumns.sColName, name);
        cvData.put(DBContract.DBColumns.sColGender, gender);
        cvData.put(DBContract.DBColumns.sColDOB, dob);
        cvData.put(DBContract.DBColumns.sColEmailAddress, emailAddress);
        cvData.put(DBContract.DBColumns.sColLocation, location);
        cvData.put(DBContract.DBColumns.sColPhoneNumber, phoneNumber);

        Log.e("DBHelper", "codeToWriteToDatabase() - Data put into ContentValues SUCCESSFULLY");

        SQLiteDatabase sqldbDatabase = this.getWritableDatabase();
        switch (tableName) {

            case DBContract.sTABLE_ATHLETE:

                sqldbDatabase.insert(DBContract.sTABLE_ATHLETE, null, cvData);
                Log.e("DBHelper", "codeToWriteToDatabase() - Data INSERTED INTO TABLE_ATHLETE SUCCESSFULLY");
                break;

            case DBContract.sTABLE_BOXER:

                sqldbDatabase.insert(DBContract.sTABLE_BOXER, null, cvData);
                Log.e("DBHelper", "codeToWriteToDatabase() - Data INSERTED INTO TABLE_BOXER SUCCESSFULLY");
                break;

            case DBContract.sTABLE_SPONSOR:

                sqldbDatabase.insert(DBContract.sTABLE_SPONSOR, null, cvData);
                Log.e("DBHelper", "codeToWriteToDatabase() - Data INSERTED INTO TABLE_SPONSOR SUCCESSFULLY");
                break;
        }
        sqldbDatabase.close();
        Log.e("DBHelper", "codeToWriteToDatabase() - SQLiteDatabase CLOSED SUCCESSFULLY");

        Log.e("DBHelper", "codeToWriteToDatabase() -- DATA WRITTEN SUCCESSFULLY!");
    }

    public ArrayList<ModelSportsMan> codeToReadFromTableSportsMans(String tableName) throws SQLException {

        Log.e("DBHelper", "codeToReadFromTableSportsMans() CALLED!");

        ArrayList<ModelSportsMan> arylSportsMans = new ArrayList<>();

        Cursor curReadCursor = null;
        SQLiteDatabase sqldbDatabase = this.getReadableDatabase();
        switch (tableName) {

            case DBContract.sTABLE_ATHLETE:
                curReadCursor = sqldbDatabase.rawQuery(DBContract.sSELECT_ALL_FROM_ATHLETE, null);
                break;

            case DBContract.sTABLE_BOXER:
                curReadCursor = sqldbDatabase.rawQuery(DBContract.sSELECT_ALL_FROM_BOXER, null);
                break;

            case DBContract.sTABLE_SPONSOR:
                curReadCursor = sqldbDatabase.rawQuery(DBContract.sSELECT_ALL_FROM_SPONSOR, null);
                break;
        }

        if (curReadCursor != null) {

            Log.e("DBHelper", "codeToReadFromTableSportsMans() - curReadCursor is NOT NULL!");

            curReadCursor.moveToFirst();

            Log.e("DBHelper", "codeToReadFromTableSportsMans() - curReadCursor moveToFirst()");

            while (!(curReadCursor.isAfterLast())) {

                ModelSportsMan clsSportsMan = new ModelSportsMan();
                clsSportsMan.setiSportsManId(curReadCursor.getInt(0));
                clsSportsMan.setsName(curReadCursor.getString(1));
                clsSportsMan.setsGender(curReadCursor.getString(2));
                clsSportsMan.setsDateOfBirth(curReadCursor.getString(3));
                clsSportsMan.setsEmailAddress(curReadCursor.getString(4));
                clsSportsMan.setsLocation(curReadCursor.getString(5));
                clsSportsMan.setsPhoneNumber(curReadCursor.getString(6));

                arylSportsMans.add(clsSportsMan);

                Log.e("DBHelper", "codeToReadFromTableSportsMans() - curReadCursor READ SUCCESSFUL. arylSportsMans: " + arylSportsMans.size());

                curReadCursor.moveToNext();

                Log.e("DBHelper", "codeToReadFromTableSportsMans() - curReadCursor moveToNext()");
            }

            curReadCursor.close();

            Log.e("DBHelper", "codeToReadFromTableSportsMans() - curReadCursor CLOSED");
        }

        return arylSportsMans;
    }

    public void codeToDeleteFromDatabase(String tableName, String[] recordIdToDelete) throws SQLException {

        Log.e("DBHelper", "codeToDeleteFromDatabase() -- CALLED!");

        SQLiteDatabase sqldbDatabase = this.getWritableDatabase();
        switch (tableName) {

            case DBContract.sTABLE_ATHLETE:
                sqldbDatabase.delete(DBContract.sTABLE_ATHLETE, DBContract.DBColumns.sColAthleteID + " = ?", recordIdToDelete);

                Log.e("DBHelper", "codeToDeleteFromDatabase() -- ATHLETE Record ID: " + recordIdToDelete[0] + " SUCCESSFULLY DELETED!");
                break;

            case DBContract.sTABLE_BOXER:
                sqldbDatabase.delete(DBContract.sTABLE_BOXER, DBContract.DBColumns.sColBoxerID + " = ?", recordIdToDelete);

                Log.e("DBHelper", "codeToDeleteFromDatabase() -- BOXER Record ID: " + recordIdToDelete[0] + " SUCCESSFULLY DELETED!");
                break;

            case DBContract.sTABLE_SPONSOR:
                sqldbDatabase.delete(DBContract.sTABLE_SPONSOR, DBContract.DBColumns.sColSponsorID + " = ?", recordIdToDelete);

                Log.e("DBHelper", "codeToDeleteFromDatabase() -- SPONSOR Record ID: " + recordIdToDelete[0] + " SUCCESSFULLY DELETED!");
                break;
        }
    }
}
