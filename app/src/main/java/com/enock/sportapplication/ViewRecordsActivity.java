package com.enock.sportapplication;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.enock.sportapplication.Adapters.AdapterViewRecords;
import com.enock.sportapplication.DBUtils.DBHelper;
import com.enock.sportapplication.Models.ModelSportsMan;

import java.sql.SQLException;
import java.util.ArrayList;

public class ViewRecordsActivity extends AppCompatActivity {

    private AdapterViewRecords clsAdapterViewRecords;

    private ListView lstViewRecords;

    private ArrayList<ModelSportsMan> arylRecords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_records);

        initializeVariablesAndUIObjects();
    }


    /**
     * Method to Declare and Initialize Variables,
     * and UI Objects.
     *
     * Called in this.onCreate()
     */
    private void initializeVariablesAndUIObjects() {

        String sTableName = this.getIntent().getStringExtra("TABLE_NAME_TO_VIEW");

        clsAdapterViewRecords = new AdapterViewRecords(ViewRecordsActivity.this, codeToFetchRecordsFromDatabase(sTableName));

        lstViewRecords = (ListView) this.findViewById(R.id.lstViewRecords);
        lstViewRecords.setOnItemClickListener(clkRecords);
        lstViewRecords.setAdapter(clsAdapterViewRecords);

    }


    /**
     * Method to fetch records from Database.
     *
     * Called in this.initializeVariablesAndUIObjects();
     *
     * @param tableName     (String)
     * @return arylDetails          (ArrayList)
     */
    private ArrayList<ModelSportsMan> codeToFetchRecordsFromDatabase(String tableName) {

        arylRecords = new ArrayList<>();
        try {
            arylRecords = new DBHelper(ViewRecordsActivity.this).codeToReadFromTableSportsMans(tableName);
        } catch (SQLException sqlex) {
            sqlex.printStackTrace();
        }

        return arylRecords;
    }


    /**
     * ListView.OnItemClickListener interface for ListView View Records.
     * Handles clicks on items of this ListView.
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private ListView.OnItemClickListener clkRecords = new ListView.OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            try {
                String sTableName = getIntent().getStringExtra("TABLE_NAME_TO_VIEW");

                int iRecordIdToDelete = arylRecords.get(position).getiSportsManId();

                String sRecordIdToDelete = Integer.toString(iRecordIdToDelete);
                String[] saryRecordIdToDelete = {String.valueOf(sRecordIdToDelete)};

                new DBHelper(ViewRecordsActivity.this).codeToDeleteFromDatabase(sTableName, saryRecordIdToDelete);

                clsAdapterViewRecords = new AdapterViewRecords(ViewRecordsActivity.this, codeToFetchRecordsFromDatabase(sTableName));
                lstViewRecords.setAdapter(clsAdapterViewRecords);
            } catch (SQLException sqlex) {
                sqlex.printStackTrace();
            }
        }
    };
}
