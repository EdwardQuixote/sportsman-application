package com.enock.sportapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initializeVariablesAndUIObjects();

    }


    /**
     * Method to declare and initialize,
     * class variables and UI Objects referenced,
     * in the UI.
     *
     * Called in this.onCreate();
     */
    private void initializeVariablesAndUIObjects() {

        Button buttonAthletes = (Button) findViewById(R.id.btnAthletes);
        Button buttonBoxers = (Button) findViewById(R.id.btnBoxers);
        Button buttonSponsors = (Button) findViewById(R.id.btnSponsor);
        buttonAthletes.setOnClickListener(clkHome);
        buttonBoxers.setOnClickListener(clkHome);
        buttonSponsors.setOnClickListener(clkHome);

    }


    /**
     * View.OnClickListener interface for Views in this Activity.
     * Handles their clicks.
     *
     * Implemented in this.initializeVariablesAndUIObjects();
     */
    private View.OnClickListener clkHome = new View.OnClickListener() {

        @Override
        public void onClick(View v) {

            switch (v.getId()) {

                case R.id.btnAthletes:
                    Intent startAthleteActivity = new Intent(HomeActivity.this, AthleteActivity.class);
                    startActivity(startAthleteActivity);
                    break;

                case R.id.btnBoxers:
                    Intent startBoxersActivity = new Intent(HomeActivity.this, BoxersActivity.class);
                    startActivity(startBoxersActivity);
                    break;

                case R.id.btnSponsor:
                    Intent inStartSponsors = new Intent(HomeActivity.this, SponsorsActivity.class);
                    startActivity(inStartSponsors);
                    break;
            }
        }
    };
}
